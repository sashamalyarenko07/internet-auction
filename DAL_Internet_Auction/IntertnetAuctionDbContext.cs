﻿using DAL_Internet_Auction.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL_Internet_Auction
{
    public class IntertnetAuctionDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Lot> Lots { get; set; }
        public DbSet<Bet> Bets { get; set; }
        public DbSet<Category> Categories { get; set; }

        public IntertnetAuctionDbContext(DbContextOptions<IntertnetAuctionDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Lot>()
                .Property(x => x.MinPriceIncrease)
                .HasDefaultValue(1);

            builder.Entity<Lot>()
                 .Property(x => x.IsClosed)
                 .HasDefaultValue(false);

        }

    }
}
