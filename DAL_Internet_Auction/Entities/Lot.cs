﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL_Internet_Auction.Entities
{
    public class Lot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool IsClosed { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal StartingPrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MinPriceIncrease { get; set; }
        [ForeignKey(nameof(Category))]
        public int CategoryId { get; set; }
        public int? BetIdWinner { get; set; }
        public string ApplicationUserId { get; set; }


        public virtual ICollection<Bet> Bets { get; set; }
        public virtual Category Category { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
