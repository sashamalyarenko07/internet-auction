﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL_Internet_Auction.Entities
{
    public class Bet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Lot))]
        public int LotId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BetSize { get; set; }
        public string ApplicationUserId { get; set; }

        public virtual Lot Lot { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
