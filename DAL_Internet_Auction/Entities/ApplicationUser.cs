﻿using Microsoft.AspNetCore.Identity;

namespace DAL_Internet_Auction.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
