﻿using DAL_Internet_Auction.Interfaces;
using DAL_Internet_Auction.Repositories;
using System.Threading.Tasks;

namespace DAL_Internet_Auction
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IntertnetAuctionDbContext _context;
        private LotRepository lotRepository;
        private BetRepository betRepository;
        private CategoryRepository categoryRepository;

        public UnitOfWork(IntertnetAuctionDbContext context)
        {
            _context = context;
        }

        public ILotRepository LotRepository
        {
            get
            {
                if (lotRepository == null)
                    lotRepository = new LotRepository(_context);
                return lotRepository;
            }
        }

        public IBetRepository BetRepository
        {
            get
            {
                if (betRepository == null)
                    betRepository = new BetRepository(_context);
                return betRepository;
            }
        }

        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (categoryRepository == null)
                    categoryRepository = new CategoryRepository(_context);
                return categoryRepository;
            }
        }
        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
