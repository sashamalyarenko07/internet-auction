﻿using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly IntertnetAuctionDbContext _context;
        public BetRepository(IntertnetAuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Bet bet)
        {
            await _context.Bets.AddAsync(bet);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(Bet bet)
        {

            var item = await _context.Bets.FirstOrDefaultAsync(x => x.Id == bet.Id);
            if (item == null)
            {
                throw new ArgumentException("Bet was not found");
            }

            _context.Bets.Remove(item);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Bets.FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
            {
                throw new ArgumentException("Bet was not found");
            }

            _context.Bets.Remove(item);

            await _context.SaveChangesAsync();

            return true;

        }

        public IQueryable<Bet> GetAll()
        {
            return _context.Bets;
        }

        public IQueryable<Bet> GetAllWithDetails()
        {

            return _context.Bets.Include(x => x.Lot);
        }

        public async Task<Bet> GetByIdAsync(int id)
        {
            var bet = await _context.Bets.FirstOrDefaultAsync(x => x.Id == id);

            if (bet == null)
            {
                throw new ArgumentException("Bet was not found");
            }

            return bet;
        }

        public async Task<Bet> GetByIdWithDetailsAsync(int id)
        {
            var bet = await _context.Bets.Include(x => x.Lot).FirstOrDefaultAsync(x => x.Id == id);

            if (bet == null)
            {
                throw new ArgumentException("Bet was not found");
            }

            return bet;
        }
        public async Task<bool> UpdateAsync(Bet bet)
        {
            var item = await GetByIdAsync(bet.Id);
            item.LotId = bet.LotId;
            item.BetSize = bet.BetSize;
            return item != null;
        }
    }
}
