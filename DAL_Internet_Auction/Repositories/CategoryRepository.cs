﻿using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IntertnetAuctionDbContext _context;
        public CategoryRepository(IntertnetAuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Category category)
        {
            _context.Categories.Add(category);

            await _context.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(Category category)
        {
            var item = await _context.Categories.FirstOrDefaultAsync(x => x.Id == category.Id);


            if (item == null)
            {
                throw new ArgumentException("Category was  not found");
            }

            _context.Categories.Remove(item);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
            {
                throw new ArgumentException("Category was  not found");
            }

            _context.Categories.Remove(item);
            await _context.SaveChangesAsync();

            return true;
        }

        public IQueryable<Category> GetAll()
        {
            return _context.Categories;
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            var item = await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
            {
                throw new ArgumentException("Category was  not found");
            }

            return item;
        }

        public async Task<bool> UpdateAsync(Category category)
        {
            var item = await GetByIdAsync(category.Id);
            item.NameCategory = category.NameCategory;
            await _context.SaveChangesAsync();
            return item != null;
        }
    }
}
