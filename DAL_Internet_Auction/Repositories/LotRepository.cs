﻿using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Repositories
{
    public class LotRepository : ILotRepository
    {
        private readonly IntertnetAuctionDbContext _context;
        public LotRepository(IntertnetAuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Lot lot)
        {
            await _context.Lots.AddAsync(lot);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(Lot lot)
        {
            var item = await _context.Lots.FirstOrDefaultAsync(x => x.Id == lot.Id);

            if (item == null)
            {
                throw new ArgumentException("Lot was  not found");
            }

            _context.Lots.Remove(item);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await _context.Lots.FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
            {
                throw new ArgumentException("Lot was not found");
            }

            _context.Lots.Remove(item);
            await _context.SaveChangesAsync();

            return true;
        }

        public IQueryable<Lot> GetAll()
        {
            return _context.Lots;
        }

        public IQueryable<Lot> GetAllWithDetails()
        {

            return _context.Lots.Include(x => x.Bets);
        }

        public async Task<Lot> GetByIdAsync(int id)
        {
            var lot = await _context.Lots.FirstOrDefaultAsync(x => x.Id == id);

            if (lot == null)
            {
                throw new ArgumentException("Lot was not found");
            }

            return lot;
        }

        public async Task<Lot> GetByIdWithDetailsAsync(int id)
        {
            var lot = await _context.Lots.Include(x => x.Bets).FirstOrDefaultAsync(x => x.Id == id);

            if (lot == null)
            {
                throw new ArgumentException("Lot was not found");
            }

            return lot;
        }

        public async Task<bool> UpdateAsync(Lot lot)
        {
            var item = await GetByIdAsync(lot.Id);
            item.IsClosed = lot.IsClosed;
            item.Title = lot.Title;
            item.Description = lot.Description;
            item.StartTime = lot.StartTime;
            item.EndTime = lot.EndTime;
            item.StartingPrice = lot.StartingPrice;
            item.MinPriceIncrease = lot.MinPriceIncrease;
            item.CategoryId = lot.CategoryId;
            await _context.SaveChangesAsync();
            return item != null;
        }
    }
}
