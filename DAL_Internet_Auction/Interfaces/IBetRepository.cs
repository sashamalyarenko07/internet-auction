﻿using DAL_Internet_Auction.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Interfaces
{
    public interface IBetRepository
    {
        IQueryable<Bet> GetAll();

        Task<Bet> GetByIdAsync(int id);

        Task AddAsync(Bet bet);

        Task<bool> UpdateAsync(Bet bet);

        Task<bool> DeleteAsync(Bet bet);

        Task<bool> DeleteByIdAsync(int id);

        Task<Bet> GetByIdWithDetailsAsync(int id);

        IQueryable<Bet> GetAllWithDetails();
    }
}
