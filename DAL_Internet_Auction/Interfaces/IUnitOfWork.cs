﻿using System.Threading.Tasks;

namespace DAL_Internet_Auction.Interfaces
{
    public interface IUnitOfWork
    {
        ILotRepository LotRepository { get; }
        IBetRepository BetRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        Task<int> SaveAsync();
    }
}
