﻿using DAL_Internet_Auction.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Interfaces
{
    public interface ICategoryRepository
    {
        Task AddAsync(Category category);
        IQueryable<Category> GetAll();
        Task<Category> GetByIdAsync(int id);
        Task<bool> UpdateAsync(Category category);
        Task<bool> DeleteByIdAsync(int id);
        Task<bool> DeleteAsync(Category category);
    }
}
