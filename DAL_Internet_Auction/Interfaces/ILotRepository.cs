﻿using DAL_Internet_Auction.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL_Internet_Auction.Interfaces
{
    public interface ILotRepository
    {
        IQueryable<Lot> GetAll();

        Task<Lot> GetByIdAsync(int id);

        Task AddAsync(Lot lot);

        Task<bool> UpdateAsync(Lot lot);

        Task<bool> DeleteAsync(Lot lot);

        Task<bool> DeleteByIdAsync(int id);

        Task<Lot> GetByIdWithDetailsAsync(int id);

        IQueryable<Lot> GetAllWithDetails();
    }
}
