﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using BLL_Internet_Auction.Services;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_Auction_Test.BLL_Internet_Auction_Test
{
    public class CategoryServiceTest
    {
        [Test]
        public async Task CategoryService_GetAll_ReturnsCategoryModels()
        {
            var expected = GetTestCategoryModels().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.CategoryRepository.GetAll())
                .Returns(GetTestCategoryEntities().AsQueryable());
            ICategoryService categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await categoryService.GetAllAsync();
            var act = actual.ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].NameCategory, act[i].NameCategory);

            }
        }

        private IEnumerable<CategoryModel> GetTestCategoryModels()
        {
            return new List<CategoryModel>()
            {
                new CategoryModel{NameCategory="Art"},
                new CategoryModel{NameCategory="Fashion"},
                new CategoryModel{NameCategory="Home and Garden"}
            };
        }

        private List<Category> GetTestCategoryEntities()
        {
            return new List<Category>()
            {
                new Category{Id=1,NameCategory="Art"},
                new Category{Id=2,NameCategory="Fashion"},
                new Category{Id=3,NameCategory="Home and Garden"}
            };
        }

        [Test]
        public async Task CategoryService_GetByIdAsync_ReturnsCategoryModel()
        {
            var expected = GetTestCategoryModels().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.CategoryRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestCategoryEntities().First);
            ICategoryService categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await categoryService.GetByIdAsync(1);

            Assert.AreEqual(expected.NameCategory, actual.NameCategory);

        }

        [Test]
        public async Task CategoryService_AddAsync_AddsModel()
        {

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.CategoryRepository.AddAsync(It.IsAny<Category>()));
            ICategoryService categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var category = new CategoryModel { NameCategory = "art" };


            await categoryService.AddAsync(category);


            mockUnitOfWork.Verify(x => x.CategoryRepository.AddAsync(It.Is<Category>(b => b.NameCategory == category.NameCategory)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [TestCase(1, "gdf")]
        [TestCase(2, "gdf")]
        [TestCase(100, "gdf")]
        public async Task CategoryService_DeleteByIdAsync_DeletesCategory(int categoryId, string userId)
        {

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.CategoryRepository.DeleteByIdAsync(It.IsAny<int>()));
            ICategoryService categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());


            await categoryService.DeleteByIdAsync(categoryId, userId);


            mockUnitOfWork.Verify(x => x.CategoryRepository.DeleteByIdAsync(categoryId), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

    }
}
