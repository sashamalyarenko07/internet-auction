﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using BLL_Internet_Auction.Services;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_Auction_Test.BLL_Internet_Auction_Test
{
    public class BetServiceTest
    {
        [Test]
        public async Task BetService_GetAll_ReturnsBetModels()
        {
            var expected = GetTestBetModels().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.BetRepository.GetAll())
                .Returns(GetTestBetEntities().AsQueryable());
            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await betService.GetAllAsync();
            var act = actual.ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].ApplicationUserId, act[i].ApplicationUserId);
                Assert.AreEqual(expected[i].LotId, act[i].LotId);
                Assert.AreEqual(expected[i].BetSize, act[i].BetSize);
            }
        }

        private IEnumerable<BetModel> GetTestBetModels()
        {
            return new List<BetModel>()
            {
                new BetModel{ApplicationUserId="1",BetSize=1000,LotId=3},
                new BetModel{ApplicationUserId="2",BetSize=4000,LotId=2},
                new BetModel{ApplicationUserId="2",BetSize=2500,LotId=2}
            };
        }

        private List<Bet> GetTestBetEntities()
        {
            return new List<Bet>()
            {
                new Bet{Id=1,ApplicationUserId="1",BetSize=1000,LotId=3},
                new Bet{Id=2,ApplicationUserId="2",BetSize=4000,LotId=2},
                new Bet{Id=3,ApplicationUserId="2",BetSize=2500,LotId=2}
            };
        }

        [Test]
        public async Task BetService_GetByIdAsync_ReturnsBetModel()
        {
            var expected = GetTestBetModels().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.BetRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestBetEntities().First);
            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await betService.GetByIdAsync(1);

            Assert.AreEqual(expected.ApplicationUserId, actual.ApplicationUserId);
            Assert.AreEqual(expected.LotId, actual.LotId);
            Assert.AreEqual(expected.BetSize, actual.BetSize);

        }

        [TestCase(1, "gdf")]
        [TestCase(2, "gdf")]
        [TestCase(100, "gdf")]
        public async Task BetService_DeleteByIdAsync_DeletesBet(int betId, string userId)
        {

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BetRepository.DeleteByIdAsync(It.IsAny<int>()));
            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());


            await betService.DeleteByIdAsync(betId, userId);


            mockUnitOfWork.Verify(x => x.BetRepository.DeleteByIdAsync(betId), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task BetService_AddAsync_AddsModel()
        {

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.LotRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestLotEntities().First);

            mockUnitOfWork.Setup(x => x.BetRepository.AddAsync(It.IsAny<Bet>()));

            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var bet = new BetModel { ApplicationUserId = "1", BetSize = 1000, LotId = 1 };

            await betService.AddAsync(bet);

            mockUnitOfWork.Verify(x => x.BetRepository.AddAsync(It.Is<Bet>(b => b.BetSize == bet.BetSize && b.LotId == bet.LotId && b.ApplicationUserId == bet.ApplicationUserId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);

        }

        private List<Lot> GetTestLotEntities()
        {
            return new List<Lot>()
            {
                new Lot{Id=1}
            };
        }

        [Test]
        public async Task BetService_UpdateAsync_UpdatesBet()
        {

            var bet = new BetModel { ApplicationUserId = "1", BetSize = 1000, LotId = 1 };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
              .Setup(m => m.LotRepository.GetByIdAsync(It.IsAny<int>()))
              .ReturnsAsync(GetTestLotEntities().First);
            mockUnitOfWork.Setup(x => x.BetRepository.UpdateAsync(It.IsAny<Bet>()));
            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());


            await betService.UpdateAsync(bet);


            mockUnitOfWork.Verify(x => x.BetRepository.UpdateAsync(It.Is<Bet>(b => b.BetSize == bet.BetSize && b.LotId == bet.LotId && b.ApplicationUserId == bet.ApplicationUserId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task BetService_GetLastBetByIdLot_ReturnsBetModel()
        {
            var expected = GetTestBetModels().Last();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                 .Setup(m => m.BetRepository.GetAll())
                 .Returns(GetTestBetEntities().AsQueryable());

            IBetService betService = new BetService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await betService.GetLastBetByIdLot(2);

            Assert.AreEqual(expected.ApplicationUserId, actual.ApplicationUserId);
            Assert.AreEqual(expected.LotId, actual.LotId);
            Assert.AreEqual(expected.BetSize, actual.BetSize);

        }
    }
}
