﻿using DAL_Internet_Auction;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Repositories;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_Auction_Test.DAL_Internet_Auction_Test
{
    [TestFixture]
    public class BetRepositoryTest
    {
        [Test]
        public void BetRepository_GetAll_ReturnsAllValues()
        {

            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var betRepository = new BetRepository(context);

                var bets = betRepository.GetAll();

                Assert.AreEqual(2, bets.Count());
            }

        }

        [Test]
        public async Task BetRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var betRepository = new BetRepository(context);

                var bet = await betRepository.GetByIdAsync(1);

                Assert.AreEqual(1, bet.Id);
                Assert.AreEqual(1, bet.LotId);
                Assert.AreEqual(1000, bet.BetSize);
            }
        }

        [Test]
        public async Task BetRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var betRepository = new BetRepository(context);
                var bet = new Bet() { Id = 3 };
                await betRepository.AddAsync(bet);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Bets.Count());
            }
        }

        [Test]
        public async Task BetRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var betRepository = new BetRepository(context);
                await betRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, context.Bets.Count());
            }
        }

        [Test]
        public async Task BetRepository_Update_UpdatesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var betRepository = new BetRepository(context);
                var bet = new Bet() { Id = 1, LotId = 2, BetSize = 2500 };
                await betRepository.UpdateAsync(bet);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, bet.Id);
                Assert.AreEqual(2, bet.LotId);
                Assert.AreEqual(2500, bet.BetSize);
            }
        }

        [Test]
        public async Task BetRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var expectedLotInBet = 1;
                var betRepository = new BetRepository(context);
                var betWithIncludes = await betRepository.GetByIdWithDetailsAsync(1);
                var actual = betWithIncludes.Lot.Id;
                Assert.AreEqual(expectedLotInBet, actual);
            }
        }

        [Test]
        public void BetRepository_FindAllWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {

                var expectedLotInBet = 1;
                var betRepository = new BetRepository(context);
                var betWithIncludes = betRepository.GetAllWithDetails();

                var actual = betWithIncludes.FirstOrDefault().Lot.Id;

                Assert.AreEqual(expectedLotInBet, actual);
            }
        }
    }
}
