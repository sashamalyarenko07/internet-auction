﻿using DAL_Internet_Auction;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Repositories;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_Auction_Test.DAL_Internet_Auction_Test
{
    [TestFixture]
    class CategoryRepositoryTest
    {
        public void CategoryRepository_GetAll_ReturnsAllValues()
        {

            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var categoryRepository = new CategoryRepository(context);

                var categorys = categoryRepository.GetAll();

                Assert.AreEqual(2, categorys.Count());
            }

        }

        [Test]
        public async Task CategoryRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var categoryRepository = new CategoryRepository(context);

                var category = await categoryRepository.GetByIdAsync(1);

                Assert.AreEqual(1, category.Id);
                Assert.AreEqual("Art", category.NameCategory);
            }
        }

        [Test]
        public async Task CategoryRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var categoryRepository = new CategoryRepository(context);
                var category = new Category() { Id = 3 };
                await categoryRepository.AddAsync(category);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Categories.Count());
            }
        }

        [Test]
        public async Task CategoryRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var categoryRepository = new CategoryRepository(context);
                await categoryRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, context.Categories.Count());
            }
        }

        [Test]
        public async Task CategoryRepository_Update_UpdatesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var categoryRepository = new CategoryRepository(context);
                var category = new Category() { Id = 1, NameCategory = "Fashion" };
                await categoryRepository.UpdateAsync(category);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, category.Id);
                Assert.AreEqual("Fashion", category.NameCategory);

            }
        }


    }
}
