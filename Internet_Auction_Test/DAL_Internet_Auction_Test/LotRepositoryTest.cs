﻿
using DAL_Internet_Auction;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Repositories;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Internet_Auction_Test.DAL_Internet_Auction_Test
{
    [TestFixture]
    public class LotRepositoryTest
    {
        [Test]
        public void LotRepository_GetAll_ReturnsAllValues()
        {

            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var lotRepository = new LotRepository(context);

                var lots = lotRepository.GetAll();

                Assert.AreEqual(2, lots.Count());
            }

        }

        [Test]
        public async Task LotRepository_GetById_ReturnsSingleValue()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var lotRepository = new LotRepository(context);

                var lot = await lotRepository.GetByIdAsync(1);

                Assert.AreEqual(1, lot.Id);
                Assert.AreEqual("Chloe Munday", lot.Title);
                Assert.AreEqual("Chloe Munday took this photograph as the sun set over West Wittering beach in West Sussex", lot.Description);
                Assert.AreEqual(new DateTime(2000, 7, 21), lot.StartTime);
                Assert.AreEqual(new DateTime(2000, 7, 23), lot.EndTime);
                Assert.AreEqual(1000, lot.StartingPrice);
                Assert.AreEqual(10, lot.MinPriceIncrease);
                Assert.AreEqual(1, lot.CategoryId);
            }
        }

        [Test]
        public async Task LotRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var lotRepository = new LotRepository(context);
                var lot = new Lot() { Id = 3 };
                await lotRepository.AddAsync(lot);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Lots.Count());
            }
        }

        [Test]
        public async Task LotRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var lotRepository = new LotRepository(context);
                await lotRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, context.Lots.Count());
            }
        }

        [Test]
        public async Task LotRepository_Update_UpdatesEntity()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var lotRepository = new LotRepository(context);
                var lot = new Lot() { Id = 1, Title = "Pulp Fiction", Description = "John Travolta" };
                await lotRepository.UpdateAsync(lot);
                await context.SaveChangesAsync();
                Assert.AreEqual(1, lot.Id);
                Assert.AreEqual("Pulp Fiction", lot.Title);
                Assert.AreEqual("John Travolta", lot.Description);
            }
        }

        [Test]
        public async Task LotRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var expectedBetsInLot = 1;
                var lotRepository = new LotRepository(context);
                var betWithIncludes = await lotRepository.GetByIdWithDetailsAsync(1);
                var actual = betWithIncludes.Bets.Count;
                Assert.AreEqual(expectedBetsInLot, actual);
            }
        }

        [Test]
        public void LotRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new IntertnetAuctionDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {

                var expectedBetsInLot = 1;
                var lotRepository = new LotRepository(context);
                var lotWithIncludes = lotRepository.GetAllWithDetails();

                var actual = lotWithIncludes.FirstOrDefault().Bets.Count;

                Assert.AreEqual(expectedBetsInLot, actual);
            }
        }

    }
}
