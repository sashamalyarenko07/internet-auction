﻿using AutoMapper;
using BLL_Internet_Auction;
using DAL_Internet_Auction;
using DAL_Internet_Auction.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Internet_Auction_Test
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<IntertnetAuctionDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<IntertnetAuctionDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new IntertnetAuctionDbContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(IntertnetAuctionDbContext context)
        {
            context.Lots.Add(new Lot
            {
                Id = 1,
                Title = "Chloe Munday",
                Description = "Chloe Munday took this photograph as the sun set over West Wittering beach in West Sussex",
                StartTime = new DateTime(2000, 7, 21),
                EndTime = new DateTime(2000, 7, 23),
                StartingPrice = 1000,
                MinPriceIncrease = 10,
                CategoryId = 1,

            });
            context.Lots.Add(new Lot
            {
                Id = 2,
                Title = "Darren Priddle",
                Description = "This photograph was taken by Mark Duffy during a visit to Whitby pier in North Yorkshire",
                StartTime = new DateTime(2000, 5, 10),
                EndTime = new DateTime(2000, 5, 15),
                StartingPrice = 100,
                MinPriceIncrease = 5,
                CategoryId = 1,

            });

            context.Categories.Add(new Category { Id = 1, NameCategory = "Art" });
            context.Categories.Add(new Category { Id = 2, NameCategory = "HomeGaden" });
            context.Bets.Add(new Bet { LotId = 1, BetSize = 1000 });
            context.Bets.Add(new Bet { LotId = 2, BetSize = 1200 });
            context.SaveChanges();
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
