﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_Internet_Auction.Controllers;


namespace Internet_Auction_Test.WebApi_Internet_Auction_Test
{
    public class LotsControllerTest
    {
        [Test]
        public async Task LotsControllet_GetAll_ReturnsLotsModel()
        {
            var expected = GetTestLotModels().ToList();

            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.GetAllAsync())
                .ReturnsAsync(GetTestLotModels());

            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var result = await controller.GetAll();

            var act = ((result.Result as OkObjectResult).Value as IEnumerable<LotModel>).ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].Title, act[i].Title);
                Assert.AreEqual(expected[i].Description, act[i].Description);
                Assert.AreEqual(expected[i].StartTime, act[i].StartTime);
                Assert.AreEqual(expected[i].EndTime, act[i].EndTime);
                Assert.AreEqual(expected[i].StartingPrice, act[i].StartingPrice);
                Assert.AreEqual(expected[i].MinPriceIncrease, act[i].MinPriceIncrease);
                Assert.AreEqual(expected[i].CategoryId, act[i].CategoryId);
                Assert.AreEqual(expected[i].ApplicationUserId, act[i].ApplicationUserId);

            }
        }

        private IEnumerable<LotModel> GetTestLotModels()
        {
            return new List<LotModel>()
            {
                new LotModel{Title="Good book",Description="Book",StartTime=new DateTime(2021/05/05), EndTime=new DateTime(2021/11/05),StartingPrice=1000, MinPriceIncrease=10, CategoryId=1, ApplicationUserId="1"},
                new LotModel{Title="Table",Description="table",StartTime=new DateTime(2021/08/06), EndTime=new DateTime(2021/12/30),StartingPrice=800, MinPriceIncrease=15, CategoryId=2, ApplicationUserId="2"},
                new LotModel{Title="Bad book",Description="Book",StartTime=new DateTime(2021/05/02), EndTime=new DateTime(2022/05/05),StartingPrice=200, MinPriceIncrease=2, CategoryId=1, ApplicationUserId="1"},
            };
        }

        [Test]
        public async Task LotsController_GetById_ReturnsLotModel()
        {
            var expected = GetTestLotModels().First();

            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestLotModels().First());

            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var result = await controller.GetById(1);

            var act = (result.Result as OkObjectResult).Value as LotModel;


            Assert.AreEqual(expected.Title, act.Title);
            Assert.AreEqual(expected.Description, act.Description);
            Assert.AreEqual(expected.StartTime, act.StartTime);
            Assert.AreEqual(expected.EndTime, act.EndTime);
            Assert.AreEqual(expected.StartingPrice, act.StartingPrice);
            Assert.AreEqual(expected.MinPriceIncrease, act.MinPriceIncrease);
            Assert.AreEqual(expected.CategoryId, act.CategoryId);
            Assert.AreEqual(expected.ApplicationUserId, act.ApplicationUserId);

        }

        [Test]
        public async Task LotsController_Add_AddsModel()
        {
            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.AddAsync(It.IsAny<LotModel>()));


            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var lot = new LotModel { Title = "Good book", Description = "Book", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(7), StartingPrice = 1000, MinPriceIncrease = 10, CategoryId = 1, ApplicationUserId = "1" };

            await controller.Add(lot);

            mockLotService.Verify(x => x.AddAsync(It.Is<LotModel>(b => b.Title == lot.Title && b.Description == lot.Description && b.StartingPrice == lot.StartingPrice && b.MinPriceIncrease == lot.MinPriceIncrease
                && b.CategoryId == lot.CategoryId && b.ApplicationUserId == lot.ApplicationUserId && b.StartTime == lot.StartTime && b.EndTime == lot.EndTime)), Times.Once);

        }

        [Test]
        public async Task LotsController_Update_UpdatesLot()
        {
            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.UpdateAsync(It.IsAny<LotModel>()));


            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var lot = new LotModel { Title = "Good book", Description = "Book", StartTime = DateTime.Now, EndTime = DateTime.Now.AddDays(7), StartingPrice = 1000, MinPriceIncrease = 10, CategoryId = 1, ApplicationUserId = "1" };

            await controller.Update(lot);

            mockLotService.Verify(x => x.UpdateAsync(It.Is<LotModel>(b => b.Title == lot.Title && b.Description == lot.Description && b.StartingPrice == lot.StartingPrice && b.MinPriceIncrease == lot.MinPriceIncrease
                && b.CategoryId == lot.CategoryId && b.ApplicationUserId == lot.ApplicationUserId && b.StartTime == lot.StartTime && b.EndTime == lot.EndTime)), Times.Once);
        }

        [TestCase(1, "gdf")]
        [TestCase(2, "gdf")]
        [TestCase(100, "gdf")]
        public async Task LotsController_Delete_DeletesLot(int lotId, string userId)
        {
            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.DeleteByIdAsync(It.IsAny<int>(), It.IsAny<string>()));


            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            await controller.Delete(lotId, userId);

            mockLotService.Verify(x => x.DeleteByIdAsync(lotId, userId), Times.Once);
        }

        [Test]
        public async Task LotsController_GetByIdBet_ReturnsLotModel()
        {
            var expected = GetTestLotModels().First();

            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.GetByIdBet(It.IsAny<int>()))
                .ReturnsAsync(GetTestLotModels().First());

            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var result = await controller.GetByIdBet(1);

            var act = (result.Result as OkObjectResult).Value as LotModel;


            Assert.AreEqual(expected.Title, act.Title);
            Assert.AreEqual(expected.Description, act.Description);
            Assert.AreEqual(expected.StartTime, act.StartTime);
            Assert.AreEqual(expected.EndTime, act.EndTime);
            Assert.AreEqual(expected.StartingPrice, act.StartingPrice);
            Assert.AreEqual(expected.MinPriceIncrease, act.MinPriceIncrease);
            Assert.AreEqual(expected.CategoryId, act.CategoryId);
            Assert.AreEqual(expected.ApplicationUserId, act.ApplicationUserId);
        }

        [Test]
        public async Task LotsController_GetByFilter_ReturnsLotsModel()
        {
            var expected = GetTestLotModels().ToList();

            var mockLotService = new Mock<ILotService>();
            mockLotService
                .Setup(m => m.GetByFilter(It.IsAny<FilterSearchModel>()))
                .ReturnsAsync(GetTestLotModels());

            var mockLogger = new Mock<ILogger<LotsController>>();

            var controller = new LotsController(mockLotService.Object, mockLogger.Object);

            var result = await controller.GetByFilter(new FilterSearchModel());

            var act = ((result.Result as OkObjectResult).Value as IEnumerable<LotModel>).ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].Title, act[i].Title);
                Assert.AreEqual(expected[i].Description, act[i].Description);
                Assert.AreEqual(expected[i].StartTime, act[i].StartTime);
                Assert.AreEqual(expected[i].EndTime, act[i].EndTime);
                Assert.AreEqual(expected[i].StartingPrice, act[i].StartingPrice);
                Assert.AreEqual(expected[i].MinPriceIncrease, act[i].MinPriceIncrease);
                Assert.AreEqual(expected[i].CategoryId, act[i].CategoryId);
                Assert.AreEqual(expected[i].ApplicationUserId, act[i].ApplicationUserId);
            }
        }
    }
}
