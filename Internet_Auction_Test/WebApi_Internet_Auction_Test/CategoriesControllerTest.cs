﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_Internet_Auction.Controllers;

namespace Internet_Auction_Test.WebApi_Internet_Auction_Test
{
    public class CategoriesControllerTest
    {
        [Test]
        public async Task CategoriesController_GetAll_ReturnsCategoriesModel()
        {
            var expected = GetTestCategoryModels().ToList();

            var mockCategoryService = new Mock<ICategoryService>();
            mockCategoryService
                .Setup(m => m.GetAllAsync())
                .ReturnsAsync(GetTestCategoryModels());

            var mockLogger = new Mock<ILogger<CategoriesController>>();

            var controller = new CategoriesController(mockCategoryService.Object, mockLogger.Object);

            var result = await controller.GetAll();

            var act = ((result.Result as OkObjectResult).Value as IEnumerable<CategoryModel>).ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].NameCategory, act[i].NameCategory);

            }
        }

        private IEnumerable<CategoryModel> GetTestCategoryModels()
        {
            return new List<CategoryModel>()
            {
                new CategoryModel{NameCategory="Art"},
                new CategoryModel{NameCategory="Fashion"},
                new CategoryModel{NameCategory="Home and Garden"},
            };
        }

        [Test]
        public async Task CategoriesController_GetById_ReturnsCategoryModel()
        {
            var expected = GetTestCategoryModels().First();

            var mockCategoryService = new Mock<ICategoryService>();
            mockCategoryService
                .Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestCategoryModels().First());

            var mockLogger = new Mock<ILogger<CategoriesController>>();

            var controller = new CategoriesController(mockCategoryService.Object, mockLogger.Object);

            var result = await controller.GetById(1);

            var act = (result.Result as OkObjectResult).Value as CategoryModel;


            Assert.AreEqual(expected.NameCategory, act.NameCategory);

        }

        [Test]
        public async Task CategoriesController_Add_AddsModel()
        {
            var mockCategoryService = new Mock<ICategoryService>();
            mockCategoryService
                .Setup(m => m.AddAsync(It.IsAny<CategoryModel>()));


            var mockLogger = new Mock<ILogger<CategoriesController>>();

            var controller = new CategoriesController(mockCategoryService.Object, mockLogger.Object);

            var bet = new CategoryModel { NameCategory = "Art" };

            await controller.Add(bet);

            mockCategoryService.Verify(x => x.AddAsync(It.Is<CategoryModel>(b => b.NameCategory == bet.NameCategory)), Times.Once);

        }

        [Test]
        public async Task CategoriesController_Update_UpdatesCategory()
        {
            var mockCategoryService = new Mock<ICategoryService>();
            mockCategoryService
                .Setup(m => m.UpdateAsync(It.IsAny<CategoryModel>()));


            var mockLogger = new Mock<ILogger<CategoriesController>>();

            var controller = new CategoriesController(mockCategoryService.Object, mockLogger.Object);

            var bet = new CategoryModel { NameCategory = "Art" };

            await controller.Update(bet);

            mockCategoryService.Verify(x => x.UpdateAsync(It.Is<CategoryModel>(b => b.NameCategory == bet.NameCategory)), Times.Once);
        }

        [TestCase(1, "gdf")]
        [TestCase(2, "gdf")]
        [TestCase(100, "gdf")]
        public async Task CategoriesController_Delete_DeletesCategory(int betId, string userId)
        {
            var mockCategoryService = new Mock<ICategoryService>();
            mockCategoryService
                .Setup(m => m.DeleteByIdAsync(It.IsAny<int>(), It.IsAny<string>()));


            var mockLogger = new Mock<ILogger<CategoriesController>>();

            var controller = new CategoriesController(mockCategoryService.Object, mockLogger.Object);

            await controller.Delete(betId, userId);

            mockCategoryService.Verify(x => x.DeleteByIdAsync(betId, userId), Times.Once);
        }

    }
}
