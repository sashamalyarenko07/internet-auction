﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_Internet_Auction.Controllers;

namespace Internet_Auction_Test.WebApi_Internet_Auction_Test
{
    public class BetsControllerTest
    {
        [Test]
        public async Task BetsController_GetAll_ReturnsBetsModel()
        {
            var expected = GetTestBetModels().ToList();

            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.GetAllAsync())
                .ReturnsAsync(GetTestBetModels());

            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            var result = await controller.GetAll();

            var act = ((result.Result as OkObjectResult).Value as IEnumerable<BetModel>).ToList();

            for (int i = 0; i < act.Count; i++)
            {
                Assert.AreEqual(expected[i].ApplicationUserId, act[i].ApplicationUserId);
                Assert.AreEqual(expected[i].LotId, act[i].LotId);
                Assert.AreEqual(expected[i].BetSize, act[i].BetSize);
            }
        }

        private IEnumerable<BetModel> GetTestBetModels()
        {
            return new List<BetModel>()
            {
                new BetModel{ApplicationUserId="1",BetSize=1000,LotId=3},
                new BetModel{ApplicationUserId="2",BetSize=4000,LotId=2},
                new BetModel{ApplicationUserId="2",BetSize=2500,LotId=2}
            };
        }

        [Test]
        public async Task BetsController_GetById_ReturnsBetModel()
        {
            var expected = GetTestBetModels().First();

            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestBetModels().First());

            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            var result = await controller.GetById(1);

            var act = (result.Result as OkObjectResult).Value as BetModel;


            Assert.AreEqual(expected.ApplicationUserId, act.ApplicationUserId);
            Assert.AreEqual(expected.LotId, act.LotId);
            Assert.AreEqual(expected.BetSize, act.BetSize);
        }

        [Test]
        public async Task BetsController_Add_AddsModel()
        {
            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.AddAsync(It.IsAny<BetModel>()));


            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            var bet = new BetModel { ApplicationUserId = "1", BetSize = 1000, LotId = 1 };

            await controller.Add(bet);

            mockBetService.Verify(x => x.AddAsync(It.Is<BetModel>(b => b.BetSize == bet.BetSize && b.LotId == bet.LotId && b.ApplicationUserId == bet.ApplicationUserId)), Times.Once);

        }

        [Test]
        public async Task BetsController_Update_UpdatesBet()
        {
            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.UpdateAsync(It.IsAny<BetModel>()));


            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            var bet = new BetModel { ApplicationUserId = "1", BetSize = 1000, LotId = 1 };

            await controller.Update(bet);

            mockBetService.Verify(x => x.UpdateAsync(It.Is<BetModel>(b => b.BetSize == bet.BetSize && b.LotId == bet.LotId && b.ApplicationUserId == bet.ApplicationUserId)), Times.Once);
        }

        [TestCase(1, "gdf")]
        [TestCase(2, "gdf")]
        [TestCase(100, "gdf")]
        public async Task BetsController_Delete_DeletesBet(int betId, string userId)
        {
            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.DeleteByIdAsync(It.IsAny<int>(), It.IsAny<string>()));


            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            await controller.Delete(betId, userId);

            mockBetService.Verify(x => x.DeleteByIdAsync(betId, userId), Times.Once);
        }

        [Test]
        public async Task BetsController_GetLastBetByIdLot_ReturnsBetModel()
        {
            var expected = GetTestBetModels().Last();

            var mockBetService = new Mock<IBetService>();
            mockBetService
                .Setup(m => m.GetLastBetByIdLot(It.IsAny<int>()))
                .ReturnsAsync(GetTestBetModels().Last());

            var mockLogger = new Mock<ILogger<BetsController>>();

            var controller = new BetsController(mockBetService.Object, mockLogger.Object);

            var result = await controller.GetLastBetByIdLot(1);

            var act = (result.Result as OkObjectResult).Value as BetModel;


            Assert.AreEqual(expected.ApplicationUserId, act.ApplicationUserId);
            Assert.AreEqual(expected.LotId, act.LotId);
            Assert.AreEqual(expected.BetSize, act.BetSize);
        }
    }
}
