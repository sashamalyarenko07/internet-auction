﻿using BLL_Internet_Auction.Models;
using BLL_Internet_Auction.Models.Account;
using DAL_Internet_Auction.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Interfaces
{
    public interface IUserService
    {
        Task Register(RegisterModel user);
        Task<ApplicationUser> Logon(LogonModel logon);
        Task CreateRole(string roleName);
        Task AssignUserToRoles(UserToRolesModel userToRole);
        Task removeUserToRoles(UserToRolesModel userToRole);
        Task<IEnumerable<string>> GetRoles(ApplicationUser user);
        Task<IEnumerable<IdentityRole>> GetRoles();
    }
}
