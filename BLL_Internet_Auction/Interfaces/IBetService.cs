﻿using BLL_Internet_Auction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Interfaces
{
    public interface IBetService : ICrud<BetModel>
    {
        Task<BetModel> GetLastBetByIdLot(int id);
        Task<IEnumerable<BetModel>> GetAllWinnerBetforAllLots();
        Task<IEnumerable<BetModel>> GetAllWinnerBetforAllLotsByUserId(string UserId);


    }
}
