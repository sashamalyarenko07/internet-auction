﻿
using BLL_Internet_Auction.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Interfaces
{
    public interface ILotService : ICrud<LotModel>
    {
        Task<LotModel> GetByIdBet(int id);
        Task<IEnumerable<LotModel>> GetByFilter(FilterSearchModel filter);
    }
}
