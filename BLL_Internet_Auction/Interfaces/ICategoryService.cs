﻿using BLL_Internet_Auction.Models;

namespace BLL_Internet_Auction.Interfaces
{
    public interface ICategoryService : ICrud<CategoryModel>
    {

    }
}
