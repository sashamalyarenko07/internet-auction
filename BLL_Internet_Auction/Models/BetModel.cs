﻿using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models
{
    public class BetModel
    {
        public int Id { get; private set; }
        [Required(ErrorMessage = "Enter your LotId")]
        public int LotId { get; set; }
        [Required(ErrorMessage = "Enter your BetSize")]
        public decimal BetSize { get; set; }
        [Required(ErrorMessage = "Enter UserId")]
        public string ApplicationUserId { get; set; }


    }
}
