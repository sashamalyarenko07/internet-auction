﻿using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models.Account
{
    public class UserToRolesModel
    {
        [Required]
        public string Id { get; set; }
        [Required, MinLength(1)]
        public string[] Roles { get; set; }
    }
}
