﻿using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models
{
    public class LogonModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
