﻿using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models.Account
{
    public class CreateRoleModel
    {
        [Required, MinLength(1), MaxLength(20)]
        public string RoleName { get; set; }
    }
}
