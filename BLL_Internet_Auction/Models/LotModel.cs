﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models
{
    public class LotModel
    {
        public int Id { get; set; }
        public bool IsClosed { get; private set; }
        [Required(ErrorMessage = "Enter your Title")]
        [MinLength(3, ErrorMessage = "MinLength title should be 3")]
        [MaxLength(50, ErrorMessage = "MaxLength title should be 50")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Enter your Description")]
        [MinLength(3, ErrorMessage = "MinLength title should be 3")]
        [MaxLength(1000, ErrorMessage = "MaxLength title should be 1000")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Enter your StartTime")]
        public DateTime StartTime { get; set; }
        [Required(ErrorMessage = "Enter your EndTime")]
        public DateTime EndTime { get; set; }
        [Required(ErrorMessage = "Enter your StartingPrice")]
        public decimal StartingPrice { get; set; }
        public decimal MinPriceIncrease { get; set; }
        public int? BetIdWinner { get; private set; }
        [Required(ErrorMessage = "Enter your CategoryId")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Enter UserId")]
        public string ApplicationUserId { get; set; }



    }
}
