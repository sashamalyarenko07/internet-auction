﻿using System.ComponentModel.DataAnnotations;

namespace BLL_Internet_Auction.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter your NameCategory")]
        [MinLength(2)]
        public string NameCategory { get; set; }
    }
}
