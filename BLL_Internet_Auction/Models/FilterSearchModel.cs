﻿using System;

namespace BLL_Internet_Auction.Models
{
    public class FilterSearchModel
    {
        public string Title { get; set; }
        public string Category { get; set; }
        public bool IsClosed { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
