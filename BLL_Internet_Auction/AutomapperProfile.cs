﻿using AutoMapper;
using BLL_Internet_Auction.Models;
using DAL_Internet_Auction.Entities;

namespace BLL_Internet_Auction
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Bet, BetModel>().ReverseMap();

            CreateMap<Lot, LotModel>().ReverseMap();

            CreateMap<Category, CategoryModel>().ReverseMap();

        }
    }
}
