﻿using DAL_Internet_Auction;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Services
{

    public class CloseLotHostedService : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory scopeFactory;
        private Timer _timer = null!;

        /// <summary>
        /// CloseLotHostedService constructor
        /// </summary>
        /// <param name="scopeFactory"></param>
        public CloseLotHostedService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        /// <summary>
        /// Start Service
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Task.CompletedTask</returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            //while (DateTime.Now.Second != 0)
            //{
            //    Task.Delay(-1);
            //}
            //Task.Delay(0);

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        /// <summary>
        /// Close all lots where time is over
        /// </summary>
        /// <param name="state"></param>
        public void DoWork(object? state)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var a = scope.ServiceProvider.GetRequiredService<IntertnetAuctionDbContext>();
                var b = a.Lots.Include(x => x.Bets).Where(x => x.EndTime < DateTime.Now && !x.IsClosed);
                if (b != null)
                {
                    foreach (var item in b)
                    {
                        item.IsClosed = true;
                        item.BetIdWinner = item.Bets.Select(x => x.Id).LastOrDefault();
                    }
                    a.SaveChanges();
                }

            }
        }

        /// <summary>
        /// Stop Service
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Task.CompletedTask</returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
