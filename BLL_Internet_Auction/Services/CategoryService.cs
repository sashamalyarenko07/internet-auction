﻿using AutoMapper;
using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CategoryModel model)
        {
            var category = _unitOfWork.CategoryRepository.GetAll().FirstOrDefault(x => x.NameCategory == model.NameCategory);

            if (model.NameCategory.Length < 2)
            {
                throw new ArgumentException("NameCategory length should be more 2");
            }

            if (category != null)
            {
                throw new ArgumentException($"Category already exist. Category(Id={category.Id},NameCategory={category.NameCategory})");
            }

            await _unitOfWork.CategoryRepository.AddAsync(_mapper.Map<Category>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId, string userId)
        {
            if (userId == null)
            {
                throw new ArgumentException("Error UserId");
            }
            await _unitOfWork.CategoryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<CategoryModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<CategoryModel>>(_unitOfWork.CategoryRepository.GetAll());
        }

        public async Task<CategoryModel> GetByIdAsync(int id)
        {
            return _mapper.Map<CategoryModel>(await _unitOfWork.CategoryRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(CategoryModel model)
        {
            var category = _unitOfWork.CategoryRepository.GetAll().FirstOrDefault(x => x.Id == model.Id);

            if (model.NameCategory.Length < 2)
            {
                throw new ArgumentException("NameCategory length should be more 2");
            }

            if (category == null)
            {
                throw new ArgumentException("Category not found");
            }

            await _unitOfWork.CategoryRepository.UpdateAsync(_mapper.Map<Category>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
