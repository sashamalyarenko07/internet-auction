﻿using AutoMapper;
using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Services
{
    public class BetService : IBetService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public BetService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(BetModel model)
        {
            var lastbet = _unitOfWork.BetRepository.GetAll().OrderBy(x => x.Id).LastOrDefault(x => x.LotId == model.LotId);
            var lot = await _unitOfWork.LotRepository.GetByIdAsync(model.LotId);

            if (lot == null)
            {
                throw new ArgumentException("LotId not found");
            }

            if (lot.IsClosed)
            {
                throw new ArgumentException("Lot is closed");
            }

            if (lot.StartTime > DateTime.Now)
            {
                throw new ArgumentException($"Lot is not Started. StartedTime({lot.StartTime})");
            }

            if (lastbet == null && model.BetSize < lot.StartingPrice)
            {
                throw new ArgumentException($"BetSize should not be less than StartingPrice ({lot.StartingPrice})");
            }

            if (lastbet != null && lastbet.BetSize > model.BetSize)
            {
                throw new ArgumentException($"There is already a top bet ({lastbet.BetSize})");
            }

            if (lastbet != null && model.BetSize - lastbet.BetSize < lot.MinPriceIncrease)
            {
                throw new ArgumentException($"The bet is less than MinPriceIncrease ({lot.MinPriceIncrease}). Last bet was ({lastbet.BetSize})");
            }


            await _unitOfWork.BetRepository.AddAsync(_mapper.Map<Bet>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId, string userId)
        {
            if (userId == null)
            {
                throw new ArgumentException("Error UserId");
            }

            await _unitOfWork.BetRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<BetModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<BetModel>>(_unitOfWork.BetRepository.GetAll());
        }

        public async Task<BetModel> GetByIdAsync(int id)
        {
            return _mapper.Map<BetModel>(await _unitOfWork.BetRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(BetModel model)
        {
            var lastbet = _unitOfWork.BetRepository.GetAll().OrderBy(x => x.Id).LastOrDefault(x => x.LotId == model.LotId);
            var lot = await _unitOfWork.LotRepository.GetByIdAsync(model.LotId);

            if (lot == null)
            {
                throw new ArgumentException("LotId not found");
            }

            if (lot.IsClosed)
            {
                throw new ArgumentException("Lot is closed");
            }

            if (lastbet == null && model.BetSize < lot.StartingPrice)
            {
                throw new ArgumentException($"BetSize should not be less than StartingPrice ({lot.StartingPrice})");
            }

            if (lastbet != null && lastbet.BetSize > model.BetSize)
            {
                throw new ArgumentException($"There is already a top bet ({lastbet.BetSize})");
            }

            if (lastbet != null && model.BetSize - lastbet.BetSize < lot.MinPriceIncrease)
            {
                throw new ArgumentException($"The bet is less than MinPriceIncrease ({lot.MinPriceIncrease}). Last bet was ({lastbet.BetSize})");
            }

            await _unitOfWork.BetRepository.UpdateAsync(_mapper.Map<Bet>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task<BetModel> GetLastBetByIdLot(int id)
        {
            var bet = _unitOfWork.BetRepository.GetAll().OrderBy(x => x.Id).Where(x => x.LotId == id).Last();
            return _mapper.Map<BetModel>(bet);
        }

        public async Task<IEnumerable<BetModel>> GetAllWinnerBetforAllLots()
        {
            var lots = _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.IsClosed && x.BetIdWinner != null);
            List<Bet> abc = new List<Bet>();
            foreach (var lot in lots)
            {
                var bet = lot.Bets.FirstOrDefault(x => x.Id == lot.BetIdWinner);
                abc.Add(bet);
            }
            var bets = abc.OrderByDescending(x => x.LotId);
            return _mapper.Map<IEnumerable<BetModel>>(bets);
        }

        public async Task<IEnumerable<BetModel>> GetAllWinnerBetforAllLotsByUserId(string userId)
        {
            var lots = _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.IsClosed && x.BetIdWinner != null);
            List<Bet> abc = new List<Bet>();
            foreach (var lot in lots)
            {
                var bet = lot.Bets.FirstOrDefault(x => x.Id == lot.BetIdWinner && x.ApplicationUserId == userId);

                if (bet != null)
                {
                    abc.Add(bet);
                }

            }
            var bets = abc.OrderByDescending(x => x.LotId);
            return _mapper.Map<IEnumerable<BetModel>>(bets);
        }
    }
}
