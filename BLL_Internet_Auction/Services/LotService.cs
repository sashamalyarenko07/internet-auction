﻿using AutoMapper;
using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using DAL_Internet_Auction.Entities;
using DAL_Internet_Auction.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL_Internet_Auction.Services
{
    public class LotService : ILotService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public LotService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task AddAsync(LotModel model)
        {
            var category = await _unitOfWork.CategoryRepository.GetAll().FirstOrDefaultAsync(x => x.Id == model.CategoryId);

            if (model.StartTime < DateTime.Now)
            {
                throw new ArgumentException($"StartTime({model.StartTime}) should be more than Time Now({DateTime.Now})");
            }

            if (model.StartTime > model.EndTime)
            {
                throw new ArgumentException($"EndTime({model.EndTime}) should be more than StartTime({model.StartTime})");
            }

            if (model.StartTime.AddMinutes(15) > model.EndTime)
            {
                throw new ArgumentException($"Lot duration must be more 15 minutes({model.StartTime})-({model.EndTime})");
            }

            if (model.StartingPrice == 0)
            {
                throw new ArgumentException("StartingPrice should be more 0");
            }

            if (category == null)
            {
                throw new ArgumentException("Category was not found");
            }

            if (model.Title.Length < 3)
            {
                throw new ArgumentException("Title should be more 3");
            }

            if (model.Description.Length < 3)
            {
                throw new ArgumentException("Descpition should be more 3");
            }

            await _unitOfWork.LotRepository.AddAsync(_mapper.Map<Lot>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            var roles = await _userManager.GetRolesAsync(user);

            var b = roles.Contains("admin");

            var lot = _unitOfWork.LotRepository.GetAll().FirstOrDefault(x => x.Id == modelId && x.ApplicationUserId == userId);

            if (!b && lot == null)
            {
                throw new ArgumentException($"You can't delet this");

            }

            if (lot.StartTime < DateTime.Now && !b)
            {
                throw new ArgumentException($"Lot already started");
            }

            await _unitOfWork.LotRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<LotModel>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<LotModel>>(await _unitOfWork.LotRepository.GetAll().ToListAsync());
        }

        public async Task<LotModel> GetByIdAsync(int id)
        {
            return _mapper.Map<LotModel>(await _unitOfWork.LotRepository.GetByIdAsync(id));
        }


        public async Task UpdateAsync(LotModel model)
        {
            var user = await _userManager.FindByIdAsync(model.ApplicationUserId);

            var roles = await _userManager.GetRolesAsync(user);

            var b = roles.Contains("admin");

            var lot = _unitOfWork.LotRepository.GetAll().FirstOrDefault(x => x.Id == model.Id);

            if (b)
            {
                model.ApplicationUserId = lot.ApplicationUserId;
            }

            if (!b && (lot == null || lot.ApplicationUserId != model.ApplicationUserId))
            {
                throw new ArgumentException($"You can't update this");

            }

            if (lot.StartTime < DateTime.Now && !b)
            {
                throw new ArgumentException($"Lot already started");
            }

            var category = await _unitOfWork.CategoryRepository.GetAll().FirstOrDefaultAsync(x => x.Id == model.CategoryId);


            if (model.StartTime > model.EndTime)
            {
                throw new ArgumentException($"EndTime({model.EndTime}) should be more than StartTime({model.StartTime})");
            }

            if (model.StartTime.AddMinutes(15) > model.EndTime)
            {
                throw new ArgumentException($"Lot duration must be more 15 minutes({model.StartTime})-({model.EndTime})");
            }

            if (model.StartingPrice == 0)
            {
                throw new ArgumentException("StartingPrice should be more 0");
            }

            if (category == null)
            {
                throw new ArgumentException("Category was not found");
            }

            if (model.Title.Length < 3)
            {
                throw new ArgumentException("Title should be more 3");
            }

            if (model.Description.Length < 3)
            {
                throw new ArgumentException("Descpition should be more 3");
            }

            await _unitOfWork.LotRepository.UpdateAsync(_mapper.Map<Lot>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task<LotModel> GetByIdBet(int id)
        {
            var lot = await _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.Bets.Any(x => x.Id == id)).FirstOrDefaultAsync();
            return _mapper.Map<LotModel>(lot);
        }

        public async Task<IEnumerable<LotModel>> GetByFilter(FilterSearchModel filter)
        {
            var lots = _unitOfWork.LotRepository.GetAll().AsEnumerable();

            if (filter.Title != null && lots != null)
            {
                lots = lots.Where(x => x.Title.ToLower().Contains(filter.Title.ToLower()));
            }

            if (filter.Category != null && lots != null)
            {
                var categories = await _unitOfWork.CategoryRepository.GetAll().Where(x => x.NameCategory.ToLower() == filter.Category.ToLower()).Select(x => x.Id).ToListAsync();
                lots = lots.Where(x => categories.Contains(x.CategoryId));
            }

            if (!filter.IsClosed && lots != null)
            {
                lots = lots.Where(x => !x.IsClosed);
            }

            if (filter.MinPrice != 0 && lots != null)
            {
                var bets = _unitOfWork.BetRepository.GetAll().Where(x => x.BetSize > filter.MinPrice).AsEnumerable().GroupBy(x => x.LotId).Select(x => x.FirstOrDefault());

                var lotids = bets.Select(x => x.LotId).ToList();

                lots = lots.Where(x => lotids.Contains(x.Id));

            }

            if (filter.MaxPrice != 0 && lots != null)
            {
                var bets = _unitOfWork.BetRepository.GetAll().AsEnumerable().OrderBy(a => a.Id).GroupBy(x => x.LotId).Select(x => x.LastOrDefault()).Where(x => x.BetSize <= filter.MaxPrice);

                var lotids = bets.Select(x => x.LotId).ToList();

                lots = lots.Where(x => lotids.Contains(x.Id));
            }

            if (filter.StartTime != DateTime.MinValue && lots != null)
            {
                lots = lots.Where(x => x.StartTime > filter.StartTime);
            }

            if (filter.EndTime != DateTime.MinValue && lots != null)
            {
                lots = lots.Where(x => x.EndTime < filter.EndTime);
            }

            return _mapper.Map<IEnumerable<LotModel>>(lots);
        }

    }
}
