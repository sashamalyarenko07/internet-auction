﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace WebApi_Internet_Auction.Filters
{
    public class ExceptionFilter : Attribute, IAsyncExceptionFilter
    {
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            var action = context.ActionDescriptor.DisplayName;
            //var callStack = context.Exception.StackTrace;
            var exceptionMessage = context.Exception.Message;

            context.Result = new ContentResult
            {
                Content = $"[ExceptionFilter]Calling {action} failed, because: {exceptionMessage}.",
                StatusCode = 500
            };
            context.ExceptionHandled = true;
        }
    }
}
