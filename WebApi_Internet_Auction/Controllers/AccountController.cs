﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using BLL_Internet_Auction.Models.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using WebApi_Internet_Auction.Helpers;

namespace WebApi_Internet_Auction.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly JwtSettings _jwtSettings;
        private readonly ILogger<AccountController> _logger;
        /// <summary>
        /// Account Constructor
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="jwtSettings"></param>
        /// <param name="logger"></param>
        public AccountController(IUserService userService, IOptionsSnapshot<JwtSettings> jwtSettings, ILogger<AccountController> logger)
        {
            _userService = userService;
            _jwtSettings = jwtSettings.Value;
            _logger = logger;
        }

        /// <summary>
        /// Register Account
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            await _userService.Register(model);
            _logger.LogInformation($"User({model.Email}) was registered ");
            return Ok();
        }

        /// <summary>
        /// Logon to Account
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code and JWT Token</returns>
        [HttpPost("logon")]
        public async Task<IActionResult> Logon(LogonModel model)
        {
            var user = await _userService.Logon(model);

            if (user is null) return BadRequest();

            var roles = await _userService.GetRoles(user);
            _logger.LogInformation($"User({model.Email}) was authorized.");
            return Ok(JwtHelper.GenerateJwt(user, roles, _jwtSettings));

        }

        /// <summary>
        /// Create Role to User
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost("createRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateRole(CreateRoleModel model)
        {
            await _userService.CreateRole(model.RoleName);
            _logger.LogInformation($"Admin created Role({model.RoleName})");
            return Ok();
        }

        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns>200 code and All Roles</returns>
        [HttpGet("getRoles")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> GetRoles()
        {
            return Ok(await _userService.GetRoles());
        }

        /// <summary>
        /// Add Role to User
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost("assignuserToRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AssignUserToRole(UserToRolesModel model)
        {
            await _userService.AssignUserToRoles(model);
            _logger.LogInformation($"Admin added Role({model.Roles}) to User({model.Id})");
            return Ok();
        }

        /// <summary>
        /// Delete Role from User
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost("removeUserToRole")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RemoveUserToRole(UserToRolesModel model)
        {
            await _userService.removeUserToRoles(model);
            _logger.LogInformation($"Admin remove Role({model.Roles}) to User({model.Id})");
            return Ok();
        }
    }
}
