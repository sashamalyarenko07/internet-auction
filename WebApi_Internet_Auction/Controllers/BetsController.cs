﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi_Internet_Auction.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class BetsController : ControllerBase
    {

        private readonly IBetService _betservice;
        private readonly ILogger<BetsController> _logger;
        /// <summary>
        /// Bets Constructor
        /// </summary>
        /// <param name="betservice"></param>
        /// <param name="logger"></param>
        public BetsController(IBetService betservice, ILogger<BetsController> logger)
        {
            _betservice = betservice;
            _logger = logger;
        }

        /// <summary>
        /// Get all Bets
        /// </summary>
        /// <returns>200 code and All Bets</returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<BetModel>>> GetAll()
        {
            return Ok(await _betservice.GetAllAsync());
        }

        /// <summary>
        /// Get Bet by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200 code and Bet</returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<BetModel>> GetById(int id)
        {
            return Ok(await _betservice.GetByIdAsync(id));
        }

        /// <summary>
        /// Add Bet
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Add([FromBody] BetModel model)
        {
            await _betservice.AddAsync(model);
            _logger.LogInformation($"User({model.ApplicationUserId}) add bet ({model.BetSize}) to lot ({model.LotId}) ");
            return Ok();
        }

        /// <summary>
        /// Delete bet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserId"></param>
        /// <returns>200 code</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(int id, string UserId)
        {
            await _betservice.DeleteByIdAsync(id, UserId);
            _logger.LogInformation($"Admin({UserId}) delete bet ({id})");
            return Ok();
        }

        /// <summary>
        /// Update Bet
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Update(BetModel model)
        {
            await _betservice.UpdateAsync(model);
            _logger.LogInformation($"User({model.ApplicationUserId}) update Bet ({model.BetSize}) to lot ({model.LotId}) ");
            return Ok();
        }

        /// <summary>
        /// Get last bet for Lot by LotId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200 code and bet</returns>
        [HttpGet("lot/{id}")]
        public async Task<ActionResult<BetModel>> GetLastBetByIdLot(int id)
        {
            return Ok(await _betservice.GetLastBetByIdLot(id));
        }

        /// <summary>
        /// Get all Winner Bet for all Lots
        /// </summary>
        /// <returns>200 code and Bets</returns>
        [HttpGet("GetAllWinnerBetforAllLots")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<BetModel>>> GetAllWinnerBetforAllLots()
        {
            return Ok(await _betservice.GetAllWinnerBetforAllLots());
        }

        /// <summary>
        /// Get all Winner Bet for all Lots where User won by UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>200 code and Bets</returns>
        [HttpGet("GetAllWinnerBetforAllLotsByUserId")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult<IEnumerable<BetModel>>> GetAllWinnerBetforAllLotsByUserId(string userId)
        {
            return Ok(await _betservice.GetAllWinnerBetforAllLotsByUserId(userId));
        }
    }
}
