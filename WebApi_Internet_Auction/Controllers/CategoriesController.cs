﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi_Internet_Auction.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _categoryservice;
        private readonly ILogger<CategoriesController> _logger;

        /// <summary>
        /// Categories Constructor
        /// </summary>
        /// <param name="categoryservice"></param>
        /// <param name="logger"></param>
        public CategoriesController(ICategoryService categoryservice, ILogger<CategoriesController> logger)
        {
            _categoryservice = categoryservice;
            _logger = logger;
        }

        /// <summary>
        /// Get all Categories
        /// </summary>
        /// <returns>200 code and Categories</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryModel>>> GetAll()
        {
            return Ok(await _categoryservice.GetAllAsync());
        }

        /// <summary>
        /// Get Category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200 code and Category</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryModel>> GetById(int id)
        {
            return Ok(await _categoryservice.GetByIdAsync(id));
        }

        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Add([FromBody] CategoryModel model)
        {
            await _categoryservice.AddAsync(model);
            _logger.LogInformation($"Admin created category {model.NameCategory}");
            return Ok();
        }

        /// <summary>
        /// Delete Category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserId"></param>
        /// <returns>200 code</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(int id, string UserId)
        {
            await _categoryservice.DeleteByIdAsync(id, UserId);
            _logger.LogInformation($"Admin({UserId}) delete category id - {id}");
            return Ok();
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Update(CategoryModel model)
        {
            await _categoryservice.UpdateAsync(model);
            _logger.LogInformation($"Admin updated category {model.NameCategory}");
            return Ok();
        }
    }
}
