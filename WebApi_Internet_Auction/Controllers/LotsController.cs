﻿using BLL_Internet_Auction.Interfaces;
using BLL_Internet_Auction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi_Internet_Auction.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class LotsController : ControllerBase
    {
        private readonly ILotService _lotservice;
        private readonly ILogger<LotsController> _logger;

        /// <summary>
        /// Lots Constructor
        /// </summary>
        /// <param name="lotservice"></param>
        /// <param name="logger"></param>
        public LotsController(ILotService lotservice, ILogger<LotsController> logger)
        {
            _lotservice = lotservice;
            _logger = logger;
        }

        /// <summary>
        /// Get all Lots
        /// </summary>
        /// <returns>200 code and Lots</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LotModel>>> GetAll()
        {
            return Ok(await _lotservice.GetAllAsync());
        }

        /// <summary>
        /// Get Lot by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200 code and Lot</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<LotModel>> GetById(int id)
        {
            return Ok(await _lotservice.GetByIdAsync(id));
        }

        /// <summary>
        /// Add Lot
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Add([FromBody] LotModel model)
        {
            await _lotservice.AddAsync(model);
            _logger.LogInformation($"User({model.ApplicationUserId}) add lot ({model.Title})");
            return Ok();
        }

        /// <summary>
        /// Delete Lot by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="UserId"></param>
        /// <returns>200 code</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Delete(int id, string UserId)
        {
            await _lotservice.DeleteByIdAsync(id, UserId);
            _logger.LogInformation($"User({UserId}) delete lot id - {id}");
            return Ok();
        }

        /// <summary>
        /// Update Lot
        /// </summary>
        /// <param name="model"></param>
        /// <returns>200 code</returns>
        [HttpPut]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> Update(LotModel model)
        {
            await _lotservice.UpdateAsync(model);
            _logger.LogInformation($"User({model.ApplicationUserId}) update lot ({model.Title})");
            return Ok();
        }

        /// <summary>
        /// Get Lot by BetId
        /// </summary>
        /// <param name="id"></param>
        /// <returns>200 code and Lot</returns>
        [HttpGet(" bet/{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<LotModel>> GetByIdBet(int id)
        {
            return Ok(await _lotservice.GetByIdBet(id));
        }

        /// <summary>
        /// Get Lots by Filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>200 code and Lots</returns>
        [HttpGet("filter/")]
        public async Task<ActionResult<IEnumerable<LotModel>>> GetByFilter([FromQuery] FilterSearchModel filter)
        {
            return Ok(await _lotservice.GetByFilter(filter));
        }
    }
}
