import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private backend = 'https://localhost:44372/api/Categories';

  constructor(public http: HttpClient) { }
  
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.backend)
  }

  getCategory(id:number):Observable<Category> {
    return this.http.get<Category>(`${this.backend}/${id}`)
  }

  addCategory(category: Category){
    return this.http.post(this.backend,category);
  }

  deleteCategory(id:number,applicationUserId:string){
    return this.http.delete(`${this.backend}/${id}?UserId=${applicationUserId}`);
  }

  updateCategory(category:Category){
    return this.http.put(this.backend,category);
  }
}
