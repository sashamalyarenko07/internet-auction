import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bet } from '../models/bet'
import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BetService {

  private backend = 'https://localhost:44372/api/Bets';

  constructor(public http: HttpClient) { }

  getAll():Observable<Bet[]>{
    return this.http.get<Bet[]>(this.backend);
  }

  getBetByLotId(id:number):Observable<Bet>{
    return this.http.get<Bet>(`${this.backend}/lot/${id}`);
  }
  
  addBet(bet: Bet){
    return this.http.post(this.backend,bet);
  }

  getBet(id:number):Observable<Bet> {
    return this.http.get<Bet>(`${this.backend}/${id}`)
  }

  deleteBet(id:number,applicationUserId:string){
    return this.http.delete(`${this.backend}/${id}?UserId=${applicationUserId}`);
  }

  GetAllWinnerBetforAllLotsByUserId(userId:string):Observable<Bet[]>{
    return this.http.get<Bet[]>(`${this.backend}/GetAllWinnerBetforAllLotsByUserId?userId=${userId}`);
  }

  GetAllWinnerBetforAllLots():Observable<Bet[]>{
    return this.http.get<Bet[]>(`${this.backend}/GetAllWinnerBetforAllLots`);
  }
}
