import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private backend = 'https://localhost:44372/Account/';
  A:any

  constructor(private http: HttpClient,private router: Router,private jwtHelper :JwtHelperService) { 

  }

  login(email: string, password: string) {
    this.http.post(this.backend+'logon', {email: email,password: password},{responseType: 'text'}).subscribe(
     x=>localStorage.setItem('access_token',x)
    )
  }

  register(email:string, pass:string, passconfirm:string, year:number, firstname:string,lastname:string){
    this.http.post(this.backend+'register',{email: email,year: year,password: pass,passwordConfirm: passconfirm,firstName: firstname,lastname: lastname}).subscribe(x=>x)
  }

  decode():[string,string,string[]]{
    var a = localStorage.getItem('access_token');

    if(a!=null){

      var tokenPayload = this.jwtHelper.decodeToken(a);
      var b = tokenPayload['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name']
      var c = tokenPayload['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier']
      var d = tokenPayload['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']
      return [b,c,d]
    }
    return ['','',[]]
  }

  logout(){
    localStorage.removeItem('access_token');
    this.router.navigate(['home'])
  }
  
  assignUserToRole(id:string,role:string){
    return this.http.post(this.backend+'assignUserToRole',{id: id, roles: [role]});
  }

  removeUserToRole(id:string,role:string){
    return this.http.post(this.backend+'removeUserToRole',{id: id, roles: [role]});
  }
}
