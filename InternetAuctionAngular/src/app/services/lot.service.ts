import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lot } from '../models/lot'
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})

export class LotService {
  private backend = 'https://localhost:44372/api/Lots';

  constructor(public http: HttpClient) { }

  getLots(): Observable<Lot[]> {
    return this.http.get<Lot[]>(this.backend)
  }

  getLot(id:number):Observable<Lot> {
    return this.http.get<Lot>(`${this.backend}/${id}`)
  }

  addLot(lot: Lot){
    return this.http.post(this.backend,lot);
  }

  deleteLot(id:number,applicationUserId:string){
    return this.http.delete(`${this.backend}/${id}?UserId=${applicationUserId}`);
  }

  updateLot(lot: Lot){
    return this.http.put(this.backend,lot);
  }
}
