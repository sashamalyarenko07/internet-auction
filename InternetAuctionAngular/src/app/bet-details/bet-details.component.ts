import { Component, Input, OnInit } from '@angular/core';
import { Bet } from '../models/bet';
import { AuthService } from '../services/auth.service';
import { BetService } from '../services/bet.service';


@Component({
  selector: 'app-bet-details',
  templateUrl: './bet-details.component.html',
  styleUrls: ['./bet-details.component.css']
})
export class BetDetailsComponent implements OnInit {

  @Input()
  id:number;

  bet: Bet;
  userRoles:string[];
  
  isAdmin:boolean;
  isUser:boolean
  
  constructor(private betService:BetService,private auth:AuthService) { }

  ngOnInit(): void {
    this.betService.getBetByLotId(this.id).subscribe(bet=>this.bet=bet);
    var decode = this.auth.decode();
    this.userRoles=decode[2];
    this.isAdmin=this.userRoles.includes('admin');
    this.isUser=this.userRoles.includes('user');
  }

}
