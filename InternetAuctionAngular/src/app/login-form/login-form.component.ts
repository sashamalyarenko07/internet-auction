import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, DoCheck {
 
  form:FormGroup
  frm:FormGroup

  userEmail:string;
  userId:string;
  userRoles:string[];
  isUser:boolean;
  isAdmin:boolean;
  isRegister:boolean;

  constructor(private auth:AuthService, private route:Router) {
    this.form = new FormGroup({
      'email': new FormControl('',Validators.required),
      'password': new FormControl('',Validators.required),
    });

    this.frm = new FormGroup({
      'email':new FormControl('',Validators.required),
      'password':new FormControl('',Validators.required),
      'passwordconfirm':new FormControl('',Validators.required),
      'year':new FormControl('',Validators.required),
      'firstname':new FormControl(''),
      'lastname':new FormControl('')
    });
  }

  ngOnInit(): void {
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
    this.isUser=this.userRoles.includes('user');
    this.isAdmin=this.userRoles.includes('admin');
  }

  ngDoCheck(): void {
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
    this.isUser=this.userRoles.includes('user');
    this.isAdmin=this.userRoles.includes('admin');
  }

  login(){
    this.auth.login(this.form.get('email')?.value,this.form.get('password')?.value);
  }

  logout(){
    this.auth.logout();
  }

  reg(){
    this.auth.register(this.frm.get('email')?.value,this.frm.get('password')?.value,this.frm.get('passwordconfirm')?.value,
    +this.frm.get('year')?.value, this.frm.get('firstname')?.value, this.frm.get('lastname')?.value);
    this.route.navigate(['/home']);
  }
}
