import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs';
import { Category } from '../models/category';
import { Lot } from '../models/lot';
import { AuthService } from '../services/auth.service';
import { CategoryService } from '../services/category.service';
import { LotService } from '../services/lot.service';

@Component({
  selector: 'app-lot-details',
  templateUrl: './lot-details.component.html',
  styleUrls: ['./lot-details.component.css']
})
export class LotDetailsComponent implements OnInit {


  lot:Lot;
  category:Category;

  lotwasdeleted:boolean;
  lotwasupdated:boolean;

  userEmail:string;
  userId:string;
  userRoles:string[];

  isAdmin:boolean;
  isUser:boolean;

  constructor(private lotService:LotService,private _route:ActivatedRoute,private auth:AuthService) {}


  ngOnInit(): void {
    const id = this._route.snapshot.paramMap.get('id');
    if(id==null) {return;}
    const i =+id;
    this.lotService.getLot(i).subscribe(lot=>this.lot=lot);
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
    this.isAdmin=this.userRoles.includes('admin');
    this.isUser=this.userRoles.includes('user');
    
  }

  del(lotId:number){
    this.lotService.deleteLot(lotId,this.userId).pipe(first()).subscribe(id=>id);
    this.lotwasdeleted=true;
  }

  convertTime(date:Date):String{
   var a = date.toString();
   let re = /T/
   return a.replace(re,' ')
  }
}
