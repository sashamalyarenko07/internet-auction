import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from '../models/category';
import { Lot } from '../models/lot';
import { AuthService } from '../services/auth.service';
import { CategoryService } from '../services/category.service';
import { LotService } from '../services/lot.service';

@Component({
  selector: 'app-lot-list',
  templateUrl: './lot-list.component.html',
  styleUrls: ['./lot-list.component.css']
})

export class LotListComponent implements OnInit {

  lots: Lot[];
  private _allLots:Lot[];
  categories:Category[]

  form: FormGroup;

  userRoles:string[];
  
  searchString:string;
  searchCategory:number;
  isOpen:boolean;
  minprice:number;
  maxprice:number;
  startTime:Date;
  endTime:Date;

  isAdmin:boolean;
  isUser:boolean
  constructor(private _lotService: LotService,private auth:AuthService,private categoryservice:CategoryService) { }

  ngOnInit(): void {
    this._lotService.getLots().subscribe(lots=>this.lots=lots);
    this.categoryservice.getCategories().subscribe(categories=>this.categories=categories);
    var decode = this.auth.decode();
    this.userRoles=decode[2];
    this.isAdmin=this.userRoles.includes('admin');
    this.isUser=this.userRoles.includes('user');
    this.form = new FormGroup({
      'search': new FormControl(''),
      'category': new FormControl(''),
      'isopen': new FormControl(''),
      'minprice': new FormControl(''),
      'maxprice': new FormControl(''),
      'startTime': new FormControl(''),
      'endTime': new FormControl(''),
    });
  }

  onSearch(){
    this.searchString=this.form.get('search')?.value;
    this.searchCategory=this.form.get('category')?.value;
    this.isOpen=this.form.get('isopen')?.value;
    this.minprice=this.form.get('minprice')?.value;
    this.maxprice=this.form.get('maxprice')?.value;
    this.startTime=this.form.get('startTime')?.value;
    this.endTime=this.form.get('endTime')?.value;
    if(!this._allLots){
      this._allLots=this.lots;
    }

    if(this.searchString){
      this.lots = this.lots.filter(x=>x.title.toLowerCase().includes(this.searchString.toLowerCase()));
    }
     
    if(this.searchCategory){
      this.lots = this.lots.filter(x=>x.categoryId==this.searchCategory);
    }
    
    if(this.isOpen){
      this.lots = this.lots.filter(x=>x.isClosed==!this.isOpen);
    }

    if(this.minprice){
      this.lots = this.lots.filter(x=>x.startingPrice>=this.minprice);
    }

    if(this.maxprice){
      this.lots = this.lots.filter(x=>x.startingPrice<=this.maxprice);
    }

    if(this.startTime){
      this.lots = this.lots.filter(x=>x.startTime>=this.startTime);
    }

    if(this.endTime){
      this.lots = this.lots.filter(x=>x.startTime<=this.endTime);
    }

  }

  Reset(){
    if(this._allLots){
      this.lots=this._allLots;
    }
    this.form.reset()
  }
}

