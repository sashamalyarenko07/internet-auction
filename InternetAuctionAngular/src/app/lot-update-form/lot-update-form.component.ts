import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs';
import { Category } from '../models/category';
import { Lot } from '../models/lot';
import { AuthService } from '../services/auth.service';
import { CategoryService } from '../services/category.service';
import { LotService } from '../services/lot.service';

@Component({
  selector: 'app-lot-update-form',
  templateUrl: './lot-update-form.component.html',
  styleUrls: ['./lot-update-form.component.css']
})
export class LotUpdateFormComponent implements OnInit {

  @Input()
  lot:Lot;
  form: FormGroup;
  lotwasupdated:boolean;
  
  userEmail:string;
  userId:string;
  userRoles:string[];

  categories: Category[];

  
  constructor(private lotservice:LotService,private categoryservice:CategoryService,private auth:AuthService) {
    this.form = new FormGroup({
      'title': new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(50)]),
      'description': new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(1000)]),
      'startTime': new FormControl('',[Validators.required]),
      'endTime': new FormControl('',[Validators.required]),
      'startingPrice': new FormControl('',[Validators.required]),
      'minPriceIncrease': new FormControl(''),
      'categoryId': new FormControl('',[Validators.required]),
    });
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
   }

  ngOnInit(): void {
    this.categoryservice.getCategories().subscribe(categories=>this.categories=categories);
    this.form.patchValue({
      'title':this.lot.title,
      'description':this.lot.description,
      'startTime':this.lot.startTime,
      'endTime':this.lot.endTime,
      'startingPrice':this.lot.startingPrice,
      'minPriceIncrease':this.lot.minPriceIncrease,
      'categoryId':this.lot.categoryId
    })
  }

  publish(){
    const title=this.form.get('title')?.value;
    const description=this.form.get('description')?.value;
    const startTime=this.form.get('startTime')?.value;
    const endTime=this.form.get('endTime')?.value;
    const startingPrice=+this.form.get('startingPrice')?.value;
    const minPriceIncrease=+this.form.get('minPriceIncrease')?.value;
    const categoryId=+this.form.get('categoryId')?.value;
    const lot = new Lot(this.lot.id,title,description,startTime,endTime,startingPrice,minPriceIncrease,categoryId,this.userId);
    this.lotservice.updateLot(lot).pipe(first()).subscribe(id=>id);
    this.lotwasupdated=true;
  }
}
