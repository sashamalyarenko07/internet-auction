import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LotUpdateFormComponent } from './lot-update-form.component';

describe('LotUpdateFormComponent', () => {
  let component: LotUpdateFormComponent;
  let fixture: ComponentFixture<LotUpdateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LotUpdateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LotUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
