import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetFormComponent } from './bet-form/bet-form.component';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { LotDetailsComponent } from './lot-details/lot-details.component';
import { LotFormComponent } from './lot-form/lot-form.component';
import { LotListComponent } from './lot-list/lot-list.component';


const routes: Routes = [

  {
    path:'home',
    component:HomeComponent
  },

  {
    path:'lots',
    component:LotListComponent
  },

  {
    path:'lot/:id',
    component:LotDetailsComponent
  },

  {
    path:'add-bet',
    component:BetFormComponent
  },
  {
    path:'add-lot',
    component:LotFormComponent
  },
  {
    path:'account',
    component:LoginFormComponent
  },
  
  {
    path:'**',
    component:HomeComponent
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
