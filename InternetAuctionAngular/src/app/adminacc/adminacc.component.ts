import { Component, OnInit } from '@angular/core';
import { Bet } from '../models/bet';
import { Category } from '../models/category';
import { AuthService } from '../services/auth.service';
import { BetService } from '../services/bet.service';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-adminacc',
  templateUrl: './adminacc.component.html',
  styleUrls: ['./adminacc.component.css']
})
export class AdminaccComponent implements OnInit {
  bets?:Bet[];
  bet?:Bet;
  betId:number;
  betIdforDelete:number;
  allBets?:Bet[];
  allCategories?:Category[];
  nameCategoryforAdd:string;
  categoryIdForDelete:number;
  idCategoryForUpdate:number;
  nameCategoryForUpdate:string;
  idForAddRole:string;
  roleForAdd:string;
  idForRemoveRole:string;
  roleForRemove:string;

  userEmail:string;
  userId:string;
  userRoles:string[];

  isGetById:boolean;
  isGet:boolean;
  isDeleteById:boolean;
  isAddCategory:boolean;
  isDeleteByIdCategory:boolean;
  isUpdateCategory:boolean;
  isAssignUserToRole:boolean;
  isRemoveUserToRole:boolean;

  constructor(private betService:BetService,private auth:AuthService, private categoryService:CategoryService) { }

  ngOnInit(): void {
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
  }

  GetAllWinnerBetforAllLots(){
    if(!this.bets){
      this.betService.GetAllWinnerBetforAllLots().subscribe(x=>this.bets=x)
    }else{
      delete this.bets;
    }
  }

  GetById(){
    if(!this.bet){
      this.betService.getBet(this.betId).subscribe(x=>this.bet=x);
    }else{
      delete this.bet;
    }

  }

  GetAll(){
    if(!this.allBets){
      this.betService.getAll().subscribe(x=>this.allBets=x);
    }else{
      delete this.allBets;
    }
  }

  DeleteById(){
    this.betService.deleteBet(this.betIdforDelete,this.userId).subscribe(x=>x);
    this.isDeleteById=false;
  }

  AddCategory(){
    const category = new Category(0,this.nameCategoryforAdd);
    this.categoryService.addCategory(category).subscribe(x=>x);
    this.isAddCategory=false;
  }

  GetAllCategories(){
    if(!this.allCategories){
      this.categoryService.getCategories().subscribe(x=>this.allCategories=x);
    }else{
      delete this.allCategories;
    }
  }

  DeleteByIdCategory(){
    this.categoryService.deleteCategory(this.categoryIdForDelete,this.userId).subscribe(x=>x);
    this.isDeleteByIdCategory=false;
  }
  
  UpdateCategory(){
    const category = new Category(this.idCategoryForUpdate,this.nameCategoryForUpdate);
    this.categoryService.updateCategory(category).subscribe(x=>x);
    this.isAddCategory=false;
  }

  AssignUserToRole(){
    this.auth.assignUserToRole(this.idForAddRole,this.roleForAdd).subscribe(x=>x);
  }

  RemoveUserToRole(){
    this.auth.removeUserToRole(this.idForRemoveRole,this.roleForRemove).subscribe(x=>x);
  }
}
