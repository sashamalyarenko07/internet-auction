export class Bet {
    id:number;
    lotId:number;
    betSize:number;
    applicationUserId;

    constructor(lotId:number,betSize:number,applicationUserId:string ){
        
       this.lotId=lotId;
       this.betSize=betSize;
       this.applicationUserId=applicationUserId;
 
    }
}