export class Lot {
    id:number;
    isClosed:boolean;
    title:string;
    description:string;
    startTime:Date;
    endTime:Date;
    startingPrice:number;
    minPriceIncrease:number;
    categoryId:number;
    betIdWinner?:number;
    applicationUserId:string;

    constructor(id:number,title: string, description:string,startTime:Date,endTime:Date, startingPrice:number, minPriceIncrease:number, categoryId:number, applicationUserId:string ){
        this.id=id;
        this.title=title;
        this.description=description;
        this.startTime=startTime;
        this.endTime=endTime;   
        this.startingPrice=startingPrice;
        this.minPriceIncrease=minPriceIncrease;
        this.categoryId=categoryId;
        this.applicationUserId=applicationUserId;
 
    }
}
