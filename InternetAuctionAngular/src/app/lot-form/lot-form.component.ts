import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs';
import { Category } from '../models/category';
import { Lot } from '../models/lot';
import { AuthService } from '../services/auth.service';
import { CategoryService } from '../services/category.service';
import { LotService } from '../services/lot.service';

@Component({
  selector: 'app-lot-form',
  templateUrl: './lot-form.component.html',
  styleUrls: ['./lot-form.component.css']
})
export class LotFormComponent implements OnInit {
  
 
  form: FormGroup;
  lotcreated:boolean;

  userEmail:string;
  userId:string;
  userRoles:string[];

  isAdmin:boolean;
  isUser:boolean;

  categories: Category[];

  constructor(private lotservice:LotService, private categoryservice:CategoryService,private auth:AuthService) {
    this.form = new FormGroup({
      'title': new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(50)]),
      'description': new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(1000)]),
      'startTime': new FormControl('',[Validators.required]),
      'endTime': new FormControl('',[Validators.required]),
      'startingPrice': new FormControl('',[Validators.required]),
      'minPriceIncrease': new FormControl(''),
      'categoryId': new FormControl('',[Validators.required]),
    });
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
    this.isAdmin=this.userRoles.includes('admin');
    this.isUser=this.userRoles.includes('user');
   }

  ngOnInit(): void {
    this.categoryservice.getCategories().subscribe(categories=>this.categories=categories);
  }

  publish(){

    const title=this.form.get('title')?.value;
    const description=this.form.get('description')?.value;
    const startTime=this.form.get('startTime')?.value;
    const endTime=this.form.get('endTime')?.value;
    const startingPrice=+this.form.get('startingPrice')?.value;
    const minPriceIncrease=+this.form.get('minPriceIncrease')?.value;
    const categoryId=+this.form.get('categoryId')?.value;
    const lot = new Lot(0,title,description,startTime,endTime,startingPrice,minPriceIncrease,categoryId,this.userId);
    this.lotservice.addLot(lot).pipe(first()).subscribe(id=>id);
    this.lotcreated=true;
  }
}
