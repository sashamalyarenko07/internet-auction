import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title="InternetAuction"
  userRoles:string[];

  isAdmin:boolean;
  isUser:boolean
  

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
    var decode = this.auth.decode();
    this.userRoles=decode[2];
    this.isAdmin=this.userRoles.includes('admin');
    this.isUser=this.userRoles.includes('user');
  }

}
