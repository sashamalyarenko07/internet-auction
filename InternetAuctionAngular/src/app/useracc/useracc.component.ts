import { Component, OnInit } from '@angular/core';
import { Bet } from '../models/bet';
import { Lot } from '../models/lot';
import { AuthService } from '../services/auth.service';
import { BetService } from '../services/bet.service';
import { LotService } from '../services/lot.service';

@Component({
  selector: 'app-useracc',
  templateUrl: './useracc.component.html',
  styleUrls: ['./useracc.component.css']
})
export class UseraccComponent implements OnInit {
  
  bets:Bet[];
  lot?:Lot;

  userEmail:string;
  userId:string;
  userRoles:string[];

  constructor(private betService:BetService,private auth:AuthService,private lotservice:LotService) { }

  ngOnInit(): void {
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
    this.GetAllWinnerBetforAllLotsByUserId();
  }

  GetAllWinnerBetforAllLotsByUserId(){
    this.betService.GetAllWinnerBetforAllLotsByUserId(this.userId).subscribe(x=>this.bets=x)
  }

  GetLotById(id:number){
    this.lotservice.getLot(id).subscribe(x=>this.lot=x)
  }

  convertTime(date:Date):String{
    var a = date.toString();
    let re = /T/
    return a.replace(re,' ')
  }

  CloseTable(){
    delete this.lot;
  }

}
