import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LotListComponent } from './lot-list/lot-list.component';
import { LotDetailsComponent } from './lot-details/lot-details.component';
import { BetDetailsComponent } from './bet-details/bet-details.component';
import { BetFormComponent } from './bet-form/bet-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LotFormComponent } from './lot-form/lot-form.component';
import { LotUpdateFormComponent } from './lot-update-form/lot-update-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthInterceptor } from './services/auth.interceptor';
import { UseraccComponent } from './useracc/useracc.component';
import { AdminaccComponent } from './adminacc/adminacc.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LotListComponent,
    LotDetailsComponent,
    BetDetailsComponent,
    BetFormComponent,
    LotFormComponent,
    LotUpdateFormComponent,
    LoginFormComponent,
    UseraccComponent,
    AdminaccComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [ 
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS }, 
    JwtHelperService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi:true
    }
   ],

  bootstrap: [AppComponent]
  
})
export class AppModule { }
