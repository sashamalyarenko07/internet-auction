import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { catchError, first } from 'rxjs';
import { Bet } from '../models/bet';
import { AuthService } from '../services/auth.service';
import { BetService } from '../services/bet.service';

@Component({
  selector: 'app-bet-form',
  templateUrl: './bet-form.component.html',
  styleUrls: ['./bet-form.component.css']
})
export class BetFormComponent implements OnInit {
  @Input()
  lotId:number;
  userEmail:string;
  userId:string;
  userRoles:string[];
  
  form: FormGroup;
  
  constructor(private betservice:BetService,private auth:AuthService) { 
    this.form = new FormGroup({
      'betSize': new FormControl('',Validators.required),
    });
  }

  ngOnInit(): void {
    var decode = this.auth.decode();
    this.userEmail=decode[0];
    this.userId=decode[1];
    this.userRoles=decode[2];
  }

  publish(){

    const betSize=+this.form.get('betSize')?.value;
    const bet = new Bet(this.lotId,betSize,this.userId);
    this.betservice.addBet(bet).pipe(first()).subscribe(id=>id);
    this.form.reset();
    window.location.reload();
  }
}
